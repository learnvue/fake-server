const jsonServer = require('json-server')
const middlewares = require('json-server-auth')
const auth = require('json-server-auth')

const app = jsonServer.create()
const router = jsonServer.router('map.json')

app.use(jsonServer.defaults())

const PORT = 5810

const rules = auth.rewriter({
	mapdata: 664,
	users: 600
})

// /!\ Bind the router db to the app
app.db = router.db

// You must apply the auth middleware before the router
app.use(rules)
app.use(auth)
app.use(router)
app.listen(PORT, () => {
	console.log(`Listening on http://localhost:${PORT}`);
})
